<?php

namespace Drupal\permission_spreadsheet\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;
use Drupal\permission_spreadsheet\PhpSpreadsheetHelperTrait;
use Drupal\permission_spreadsheet\RoleLoaderTrait;
use Drupal\user\PermissionHandlerInterface;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Provides permission export form.
 */
class ExportForm extends FormBase {

  use RoleLoaderTrait;
  use PhpSpreadsheetHelperTrait;

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new ExportForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, ModuleHandlerInterface $module_handler, Token $token, FileSystemInterface $file_system) {
    $this->permissionHandler = $permission_handler;
    $this->moduleHandler = $module_handler;
    $this->token = $token;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions'),
      $container->get('module_handler'),
      $container->get('token'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'permission_spreadsheet_export_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('permission_spreadsheet.settings');

    $form['format'] = [
      '#type' => 'select',
      '#title' => $this->t('Format'),
      '#options' => [
        'xlsx' => $this->t('Excel 2007 (.xlsx)'),
        'xls' => $this->t('Excel 2000/2002/2003 (.xls)'),
        'ods' => $this->t('OpenDocument spreadsheet (.ods)'),
        'csv' => $this->t('Comma separated (.csv)'),
        'tsv' => $this->t('Tab separated (.tsv)'),
      ],
      '#required' => TRUE,
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
    ];
    if ($this->moduleHandler->moduleExists('token')) {
      $form['settings']['tokens'] = [
        '#theme' => 'token_tree_link',
        '#global_types' => TRUE,
      ];
    }
    $form['settings']['filename'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Filename'),
      '#default_value' => $config->get('export.filename'),
      '#description' => $this->t("Specify default filename (without extension) on download file. e.g., 'permissions'. You can use tokens."),
      '#required' => TRUE,
    ];
    $form['settings']['sheet_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sheet title'),
      '#default_value' => $config->get('export.sheet_title'),
      '#description' => $this->t("Specify default title for Excel sheet. e.g., 'Permissions'. You can use tokens."),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Download'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Output spreadsheet data.
    $format = $form_state->getValue('format');
    $temppath = $this->fileSystem->realpath($this->fileSystem->tempnam('temporary://', 'permissions'));
    $sheet_title = $this->token->replace($form_state->getValue('sheet_title'));

    try {
      $spreadsheet = $this->createSpreadsheet();
      $spreadsheet->getActiveSheet()->setTitle($sheet_title);
      $this->createWriter($format, $spreadsheet)->save($temppath);
    }
    catch (\Exception $ex) {
      $this->messenger()->addError($this->t('An error has occurred while writing spreadsheet file. @error', ['@error' => $ex->getMessage()]));
      return;
    }

    $filename = $this->token->replace($form_state->getValue('filename'));
    $response = new BinaryFileResponse($temppath);
    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename . '.' . $format);
    $response->deleteFileAfterSend(TRUE);

    $form_state->setResponse($response);
  }

  /**
   * Creates permission spreadsheet.
   *
   * @return \PhpOffice\PhpSpreadsheet\Spreadsheet
   *   A spreadsheet object.
   */
  protected function createSpreadsheet() {
    $config = $this->config('permission_spreadsheet.settings');
    $text_granted = $config->get('export.text_granted');
    $text_revoked = $config->get('export.text_revoked');

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    // Set header data.
    $system_column = 1;
    $set_header_cell = function ($column, $text) use ($sheet) {
      $sheet->getColumnDimensionByColumn($column)->setAutoSize(TRUE);
      $sheet->setCellValueByColumnAndRow($column, 1, $text);

      $style = $sheet->getStyleByColumnAndRow($column, 1);
      $style->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
      $style->getFont()->setBold(TRUE);
    };

    $set_header_cell($system_column++, $this->t('(Module Name)'));
    $set_header_cell($system_column++, $this->t('(Permission Title)'));
    $set_header_cell($system_column++, $this->t('(Module)'));
    $set_header_cell($system_column++, $this->t('(Permission)'));

    // Set body data.
    $role_names = [];
    $role_permissions = [];
    $column = $system_column;
    foreach ($this->loadNonAdminRoles() as $rid => $role) {
      $role_names[$rid] = $role->label();
      $role_permissions[$rid] = $role->getPermissions();

      $sheet->setCellValueByColumnAndRow($column, 1, $rid);
      $sheet->getColumnDimensionByColumn($column)->setAutoSize(TRUE);
      $sheet->getStyleByColumnAndRow($column, 1)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
      $column++;
    }
    $sheet->getStyle(Coordinate::stringFromColumnIndex($system_column) . ':' . Coordinate::stringFromColumnIndex($column))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

    $permissions = $this->permissionHandler->getPermissions();
    $module_names = [];
    $row = 2;
    foreach ($permissions as $permission_name => $permission) {
      $provider = $permission['provider'];
      if (!isset($module_names[$provider])) {
        $module_names[$provider] = $this->moduleHandler->getName($provider);
      }

      $column = 1;
      $sheet->setCellValueByColumnAndRow($column++, $row, $module_names[$provider]);
      $sheet->setCellValueByColumnAndRow($column++, $row, strip_tags((string) $permission['title']));
      $sheet->setCellValueByColumnAndRow($column++, $row, $provider);
      $sheet->setCellValueByColumnAndRow($column++, $row, $permission_name);

      foreach (array_keys($role_names) as $role) {
        $has_permission = in_array($permission_name, $role_permissions[$role]);
        $sheet->setCellValueByColumnAndRow($column, $row, $has_permission ? $text_granted : $text_revoked);
        $column++;
      }

      $row++;
    }

    // Add borders.
    $sheet->getStyle('D1:D' . ($row - 1))->getBorders()->getRight()->setBorderStyle(Border::BORDER_THIN);
    $sheet->getStyle('A1:' . Coordinate::stringFromColumnIndex($column - 1) . '1')->getBorders()->getOutline()->setBorderStyle(Border::BORDER_THIN);
    $sheet->getStyle('A2:' . Coordinate::stringFromColumnIndex($column - 1) . ($row - 1))->getBorders()->getOutline()->setBorderStyle(Border::BORDER_THIN);

    return $spreadsheet;
  }

}
