<?php

namespace Drupal\Tests\permission_spreadsheet\Functional;

use Drupal\file\Entity\File;
use Drupal\permission_spreadsheet\PhpSpreadsheetHelperTrait;
use Drupal\permission_spreadsheet\RoleLoaderTrait;
use Drupal\user\Entity\Role;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

/**
 * Tests import form.
 *
 * @group permission_spreadsheet
 */
class ImportFormTest extends FormTestBase {

  use RoleLoaderTrait;
  use PhpSpreadsheetHelperTrait;

  /**
   * The path of the form page.
   */
  const PAGE_PATH = 'admin/people/permissions/spreadsheet/import';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->moduleConfig->set('import.text_revoked', "N\nx");
    $this->moduleConfig->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function doFormatSpecificTest($format) {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);
    $current_user_rid = $this->adminUser->getRoles(TRUE)[0];

    // Create admin role.
    Role::create([
      'id' => 'administrator',
      'label' => 'Administrator',
      'is_admin' => TRUE,
    ])->save();

    // Create test spreadsheet.
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $sheet->setCellValueByColumnAndRow(5, 1, $current_user_rid);
    $sheet->setCellValueByColumnAndRow(4, 2, 'administer menu');
    $sheet->setCellValueByColumnAndRow(5, 2, 'Y');
    $sheet->setCellValueByColumnAndRow(4, 3, 'administer modules');
    $sheet->setCellValueByColumnAndRow(5, 3, 'N');
    $sheet->setCellValueByColumnAndRow(4, 4, 'administer site configuration');
    $sheet->setCellValueByColumnAndRow(5, 4, 'x');
    $sheet->setCellValueByColumnAndRow(4, 5, 'administer themes');
    $sheet->setCellValueByColumnAndRow(6, 1, 'administrator');

    $file_params = [
      'filename' => 'permission_spreadsheet_import.' . $format,
      'uri' => 'temporary://permission_spreadsheet_import.' . $format,
    ];
    $file = File::create($file_params);
    $file_real_path = $this->container->get('file_system')->realPath($file->getFileUri());
    $this->createWriter($format, $spreadsheet)->save($file_real_path);
    $file->save();
    $assert_session->assert($file->id(), 'Save import file to temporary directory.');

    // Submit import form.
    $edit = ['files[file]' => $file_real_path];
    $this->drupalPostForm(static::PAGE_PATH, $edit, t('Import'));
    $assert_session->statusCodeEquals(200);
    $assert_session->responseContains((string) t('The permissions have been imported.'));

    // Check updated permissions.
    $roles = $this->loadRoles();
    $assert_session->assert($roles['administrator']->hasPermission('administer menu'), "Check uneditable permission is protected.");

    $role = $roles[$current_user_rid];
    $assert_session->assert($role->hasPermission('administer menu'), "Check behavior for cell filled with 'Y'.");
    $assert_session->assert(!$role->hasPermission('administer modules'), "Check behavior for cell filled with 'N'.");
    $assert_session->assert(!$role->hasPermission('administer site configuration'), "Check behavior for cell filled with 'x'.");
    $assert_session->assert(!$role->hasPermission('administer themes'), "Check behavior for empty cell.");

    // Revert changes.
    user_role_change_permissions($current_user_rid, [
      'administer menu' => 0,
      'administer modules' => 0,
      'administer site configuration' => 0,
      'administer themes' => 0,
    ]);
  }

}
