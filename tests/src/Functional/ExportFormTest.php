<?php

namespace Drupal\Tests\permission_spreadsheet\Functional;

use Drupal\file\Entity\File;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Tests export form.
 *
 * @group permission_spreadsheet
 */
class ExportFormTest extends FormTestBase {

  /**
   * The path of the form page.
   */
  const PAGE_PATH = 'admin/people/permissions/spreadsheet/export';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->moduleConfig->set('export.text_revoked', 'N');
    $this->moduleConfig->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function doFormatSpecificTest($format) {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    // Submit export form.
    $edit = [];
    $edit['format'] = $format;
    $this->drupalPostForm(static::PAGE_PATH, $edit, t('Download'));
    $assert_session->statusCodeEquals(200);
    $assert_session->responseHeaderContains('Content-Disposition', 'attachment; filename=' . $this->moduleConfig->get('export.filename') . '.' . $format . '');

    // Save the exported file.
    $file_params = [
      'filename' => 'permission_spreadsheet_export.' . $format,
      'uri' => 'temporary://permission_spreadsheet_export.' . $format,
      'filemime' => $this->getSession()->getResponseHeader('Content-Type'),
    ];
    $file = File::create($file_params);
    file_put_contents($file->getFileUri(), $this->getSession()->getDriver()->getContent());
    $file->save();
    $assert_session->assert($file->id(), 'Save exported file to temporary directory.');

    // Test the exported file.
    $sheet = NULL;
    try {
      $reader = IOFactory::load($this->container->get('file_system')->realPath($file->getFileUri()));
      $sheet = $reader->getActiveSheet();
    }
    catch (\Exception $ex) {
      $assert_session->assert($sheet !== NULL, 'Load exported file.');
    }

    $assert_session->assert($sheet->getCellByColumnAndRow(7, 1)->getValue() == $this->adminUser->getRoles(TRUE)[0], 'Role name is output correctly.');

    $values = [];
    for ($row = 2; strlen($permission = $sheet->getCellByColumnAndRow(4, $row)->getValue()); $row++) {
      $values[$permission] = trim($sheet->getCellByColumnAndRow(7, $row)->getValue());
    }
    $assert_session->assert($values['access administration pages'] == $this->moduleConfig->get('export.text_granted'), 'Granted permission is output correctly.');
    $assert_session->assert($values['administer site configuration'] == $this->moduleConfig->get('export.text_revoked'), 'Revoked permission is output correctly.');
  }

}
